﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Interfaces;
using Models;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Services
{
    public class ClientService : IClient
    {
        private HttpClient http;
        private string api = "client";

        public ClientService(HttpClient client)
        {
            http = client;
        }

        public async Task delete(string id)
        {
            try
            {
                await http.DeleteAsync($"{api}/{id}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public async Task<Client> GetClientById(string id)
        {
            try
            {
                return JsonConvert.DeserializeObject<Client>(
                    await http.GetStringAsync($"{api}/{id}"));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        public async Task<IEnumerable<Client>> getClients()
        {
            try
            {
                return JsonConvert.DeserializeObject<IEnumerable<Client>>(
                    await http.GetStringAsync(api));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        public async Task insert(Client client)
        {
            try
            {
                var json = new StringContent(JsonSerializer.Serialize(
                    client), Encoding.UTF8, "application/json");
                await http.PostAsync(api, json);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public async Task update(Client client)
        {
            try
            {
                var json = new StringContent(JsonSerializer.Serialize(client),
                    Encoding.UTF8, "application/json");
                await http.PutAsync($"{api}/{client.idClient}", json);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
