﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
using Models;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Services
{
    public class LoanService : ILoan
    {
        private HttpClient http;
        private string api = "loan";
        public LoanService(HttpClient client)
        {
            http = client;
        }

        public async Task delete(string id)
        {
            try
            {
                await http.DeleteAsync($"{api}/{id}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public async Task<IEnumerable<Loan>> getAll()
        {
            try
            {
                return JsonConvert.DeserializeObject<IEnumerable<Loan>>(
                    await http.GetStringAsync(api));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        public async Task<Loan> getById(string id)
        {
            try
            {
                return JsonConvert.DeserializeObject<Loan>(
                    await http.GetStringAsync($"{api}/{id}"));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        public async Task save(Loan loan)
        {
            try
            {
                var json = new StringContent(JsonSerializer.Serialize(loan),
                    Encoding.UTF8, "application/json");
                await http.PostAsync(api, json);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public async Task update(Loan loan)
        {
            var json = new StringContent(JsonConvert.SerializeObject(loan),
                Encoding.UTF8, "application/json");
            await http.PutAsync($"{api}/{loan.Id}", json);
        }
    }
}
