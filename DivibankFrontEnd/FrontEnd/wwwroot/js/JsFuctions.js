﻿function deleteJs(text, msg, action) {
    return new Promise((resolve) => {
        Swal.fire({
            title: text,
            text: msg,
            type: action,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Delete the file'
        }).then((result) => {
            if (result.value) {
                resolve(true);
            } else {
                resolve(false);
            }
        });
    });
}