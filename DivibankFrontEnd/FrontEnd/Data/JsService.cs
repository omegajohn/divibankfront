﻿using System;
using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace FrontEnd.Data
{
    public static class JsService
    {
        public async static Task<bool> deleteSwal(
            this IJSRuntime js, string text, string msg, Actions action)
        {
            return await js.InvokeAsync<bool>("deleteJs", text, msg, action.ToString());
        }

        public enum Actions
        {
            success, warning, info, error, question
        }
    }
}
