﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models;

namespace Interfaces
{
    public interface IClient
    {
        Task<IEnumerable<Client>> getClients();
        Task<Client> GetClientById(string id);
        Task insert(Client client);
        Task update(Client client);
        Task delete(string id);
    }
}
