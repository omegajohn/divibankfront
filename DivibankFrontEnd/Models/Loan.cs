﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Loan
    {
        public Loan()
        {
            Id = Guid.NewGuid().ToString();
            created = DateTime.Now;
            Loans = new List<DataLoan>();
        }
        [Key]
        public string Id { get; set; }
        [Required]
        public string client { get; set; }
        public DateTime created { get; set; }
        public List<DataLoan> Loans { get; set; }
    }
}
