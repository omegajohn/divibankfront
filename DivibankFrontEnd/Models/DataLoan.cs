﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models
{
    public class DataLoan
    {
        public DataLoan()
        {
            Id = Guid.NewGuid().ToString().Substring(28);
        }
        [Key]
        public int idDataLoan { get; set; }
        [Required(ErrorMessage = "The field requested date is required")]
        public DateTime requested_date { get; set; }
        [Required(ErrorMessage = "The requested amount field is required")]
        [Range(0, double.MaxValue)]
        //[RegularExpression("([0-9]+)", ErrorMessage = "Please enter valid numbers only")]
        public Double requested_amount { get; set; }
        [NotMapped]
        public string Id { get; set; }
        public string LoanId { get; set; }
    }
}
