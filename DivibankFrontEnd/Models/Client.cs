﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Client
    {
        public Client()
        {
            idClient = Guid.NewGuid().ToString();
        }
        [Key]
        public string idClient { get; set; }
        [Required(ErrorMessage = "The name field is required")]
        public string name { get; set; }
        [Required(ErrorMessage = "The birthday field is required")]
        [DataType(DataType.Date)]
        public DateTime birthday { get; set; }
    }
}
